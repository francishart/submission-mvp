/**
 * Product Factory
 *
 * @description Handles Product
 */
(function() {
  'use strict';

  angular
    .module('starter.services')
    .factory('ProductService', ProductService);

  ProductService.$inject = ['$rootScope', '$q', '_', 'devUtils', 'SyncService', 'NotificationService', 'UserService'];

  function ProductService($rootScope, $q, _, devUtils, SyncService, NotificationService, UserService) {

  	var recTypeIdTime = "012R00000009BycIAE";
	  var recTypeIdExp  = "012R00000009ByhIAE";

	  var mockProducts = [{
	  		productId: '0',
	  		barcodeId: '0123456789',
	  		name: 'STIMULAN Kit 5cc',
			description: '5cc',
			price: '0.50'
	  	},
	  	{
	  		productId: '1',
	  		barcodeId: '0123456789',
	  		name: 'STIMULAN Kit 10cc',
			description: '10cc',
			price: '0.50'
	  	},
	  	{
	  		productId: '2',
	  		barcodeId: '0123456789',
	  		name: 'STIMULAN Rapid Cure 5cc',
			description: '5cc',
			price: '0.50'
	  	},
	  	{
	  		productId: '3',
	  		barcodeId: '0123456789',
	  		name: 'STIMULAN Rapid Cure 10cc',
			description: '10cc',
			price: '0.50'
	  	},
	  	{
	  		productId: '4',
	  		barcodeId: '0123456789',
	  		name: 'STIMULAN Rapid Cure 20cc',
			description: '20cc',
			price: '0.50'
	  	},
	  	{
	  		productId: '5',
	  		barcodeId: '0123456789',
	  		name: 'geneX 5cc',
			description: '5cc',
			price: '0.50'
	  	},
	  	{
	  		productId: '6',
	  		barcodeId: '0123456789',
	  		name: 'geneX 10cc',
			description: '10cc',
			price: '0.50'
	  	},
	  	{
	  		productId: '7',
	  		barcodeId: '0123456789',
	  		name: 'geneX 20cc',
			description: '20cc',
			price: '0.50'
	  	},
	  	{
	  		productId: '8',
	  		barcodeId: '0123456789',
	  		name: 'Allogran-R',
			description: 'Allogran-R',
			price: '0.50'
	  	},
	  	{
	  		productId: '9',
	  		barcodeId: '0123456789',
	  		name: 'Bilok',
			description: 'Bilok',
			price: '0.50'
	  	},
	  	{
	  		productId: '9',
	  		barcodeId: '0123456789',
	  		name: 'Biosteon',
			description: 'Biosteon',
			price: '0.50'
	  	}
	  ];
	  var product = null;

	  return {
	    all: getProducts,

	    get: function(productId) {
	      var ProductArr =  _.where(mockProducts, {'productId': productId});
	      product = ProductArr[0];
	      return product;
	    },
	    update: function(product) {
	      
	    },

	    expenses: getTimeExpense,

	    newExpense: function(varNewExp, success, error) {
	      devUtils.insertRecord('MC_Time_Expense__ap',varNewExp).then(function(res) {
	        success(res);
	        // perform background sync
	        SyncService.syncTables(['MC_Time_Expense__ap'], true);
	      }).catch(function(e) {
	        error(e);
	      });
	    },

	    getProductTotals: getProductTotals

	  };


	  function getProductsFromSmartStore() {
	    return new Promise(function(resolve, reject) {
	    	resolve(mockProducts);
	      // devUtils.readRecords('MC_Product__ap', []).then(function(resObject) {
	      //   //var records = _.sortBy(resObject.records, 'Name');
	      //   $rootScope.$broadcast('scroll.refreshComplete');
	      //   resolve(mockProducts);
	      // }).catch(function(resObject){
	      //   //console.log('Angular : Error from querySoupRecords -> ' + angular.toJson(resObject));
	      //   reject(resObject);
	      // });
	    });
	  }

	  function getProducts(refreshFlag, localProjCB) {
	    return new Promise(function(resolve, reject) {
	      	resolve(mockProducts);
	    });
	  }


	  function getLocationFromId(locationId) {
	    //console.log('Angular: locationId->' + locationId);
	    var location =  _.where($rootScope.locations, {'Id': locationId});
	    if (typeof location[0]!= 'undefined') {
	      //console.log('Angular: location->' + location[0].Name);
	      return location[0].Name;
	    } else {
	      //console.log('Angular: no location yet');
	      return '-';
	    }
	  }

	  function getLocations(locationId) {
	    return new Promise(function(resolve, reject) {
		    //console.log('Angular: getLocations');
		    devUtils.readRecords('MC_Product_Location__ap', []).then(function(resObject) {
		      // $j.each(resObject.records, function(i,record) {
		      //   $rootScope.locations.push(record);
		      // });
		      //console.log('Angular: ' + angular.toJson(locations));
		      // if (locationId != "dummy") {
		      //   $rootScope.$apply(function(){
		      //     product.location = getLocationFromId(locationId);
		      //     this.product = product;
		      //   });
		      // }
		      resolve(resObject.records);
		    }).catch(function(resObject){
		      console.error('Angular : Error from readRecords MC_Product_Location__ap -> ', resObject);
	      	reject('error', resObject);
		    });
	  	});
	  }


	   /**
	   * Returns expenses record for the product and type
	   * @param  type : 'time' | 'expense'
	   * @param  productId : string()
	   * @return promise : array of expenses recs
	   */
	  function getTimeExpense(type, productId) {
	    return new Promise(function(resolve, reject) {
	      //console.log('Angular: getTimeExpense');
	      var timeExpense = [];
	      devUtils.readRecords('MC_Time_Expense__ap', []).then(function(resObject) {
	        resObject.records.forEach(function(record) {
	          timeExpense.push(record);
	        });
	        //console.log('Angular: timeExpense' + angular.toJson(timeExpense));
	        //console.log('Angular: productId' + productId);
	        var timeExpense1 =  [];
	        if (type == "time") {
	          timeExpense1 = timeExpense.filter(function(el){
	            return (el.mobilecaddy1__Duration_Minutes__c !== null &&
	                    typeof(el.mobilecaddy1__Duration_Minutes__c) != "undefined") &&
	                    el.mobilecaddy1__Product__c == productId;
	          });
	        } else {
	          timeExpense1 = timeExpense.filter(function(el){
	            return (el.mobilecaddy1__Expense_Amount__c !== null &&
	                    typeof(el.mobilecaddy1__Expense_Amount__c) != "undefined") &&
	                    el.mobilecaddy1__Product__c == productId;
	          });
	        }

	        //console.log('Angular: timeExpense1' + angular.toJson(timeExpense1));
	        resolve(timeExpense1);
	      }).catch(function(resObject){
	        console.error('Angular : Error from readRecords MC_Time_Expense__ap -> ' + angular.toJson(resObject));
	        reject('error');
	      });
	    });
	  }

	  function getProductTotals(productId) {
	    return new Promise(function(resolve, reject) {
	      //console.log('Angular: getProductTotals',productId);
	      var totalExpense = 0;
	      var totalTime = 0;
	      devUtils.readRecords('MC_Time_Expense__ap', []).then(function(resObject) {
	        var records = _.where(resObject.records, {'mobilecaddy1__Project__c': productId});
	        //console.log('Angular: getProductTotals',records);
	        _.each(records, function(el) {
	          if (el.mobilecaddy1__Duration_Minutes__c !== null &&
	              typeof(el.mobilecaddy1__Duration_Minutes__c) != "undefined") {
	            totalTime += el.mobilecaddy1__Duration_Minutes__c;
	          } else {
	            if (el.mobilecaddy1__Expense_Amount__c !== null &&
	                typeof(el.mobilecaddy1__Expense_Amount__c) != "undefined") {
	              totalExpense += el.mobilecaddy1__Expense_Amount__c;
	            }
	          }
	        });
	        var result = {"totalExpense": totalExpense, "totalTime": totalTime};
	        resolve(result);
	      }).catch(function(resObject){
	        console.error('Error from readRecords', angular.toJson(resObject));
	        reject(resObject);
	      });
	    });
	  }

  }

})();