/**
 * Product Factory
 *
 * @description Handles Product
 */
(function() {
  'use strict';

  angular
    .module('starter.services')
    .factory('OrderService', OrderService);

  OrderService.$inject = ['$rootScope', '$q', '_', 'devUtils', 'SyncService', 'NotificationService', 'UserService', '$location'];

  function OrderService($rootScope, $q, _, devUtils, SyncService, NotificationService, UserService, $location) {

 	  var selectedSurgeon = "Select a surgeon";
 	  var selectedProduct = "Select a product";

	  return {

	  	getSelectedSurgeon: function () {
	  		return selectedSurgeon;
	  	},

	  	getSelectedProduct: function () {
	  		return selectedProduct;
	  	},

	  	setSelectedSurgeon: function (selection) {
	  		selectedSurgeon = selection;
	  	},

	  	setSelectedProduct: function (product) {
	  		selectedProduct = product;
	  	}

	  };

  }

})();