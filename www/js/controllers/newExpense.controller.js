/**
 * NewExpense Controller
 *
 * @description controller for new expenses
 */
(function() {
  'use strict';

  angular
    .module('starter.controllers')
    .controller('NewExpenseCtrl', NewExpenseCtrl);

  NewExpenseCtrl.$inject = ['$scope', '$rootScope', '$stateParams', '$ionicLoading', '$ionicPopup', '$ionicModal', '$location', '$cordovaBarcodeScanner', 'ProjectService', 'Camera', 'OrderService', 'ProductService'];

  function NewExpenseCtrl($scope, $rootScope, $stateParams, $ionicLoading, $ionicPopup, $ionicModal, $location, $cordovaBarcodeScanner, ProjectService, Camera, OrderService, ProductService) {

      $scope.selectedSurgeon = "Select a surgeon";
      $scope.selectedProduct = "Select a product";

      $scope.forms = {};

      $rootScope.$on('surgeon-updated', function(event, args) {
		    $scope.selectedSurgeon = OrderService.getSelectedSurgeon().mobilecaddy1__Short_Description__c + ', ' + OrderService.getSelectedSurgeon().mobilecaddy1__Expense_Type__c;
		});

      $rootScope.$on('product-updated', function(event, args) {
		    $scope.selectedProduct = OrderService.getSelectedProduct().name;
		});

  	  $scope.showSurgeons = function() {
  	  	$location.path('tab/project/order/new/' +  $scope.project.Id + '/surgeons');
  	  };

  	  $scope.showProducts = function() {
  	  	$location.path('tab/project/order/new/' +  $scope.project.Id + '/products');
  	  };

	  switch ($stateParams.type) {
	      case 'order' :
	        $scope.paramType = "Order";
	        break;
	      case 'surgeon' :
	      	$scope.paramType = "Surgeon";
	      	break;
	      default :
	        $scope.paramType = 'Other';
	    }

	    console.log("Statetype: " + $stateParams.type);
  		console.log("paramtype: " + $scope.paramType);
  		 console.log("stateparams: " + JSON.stringify($stateParams));



	  $scope.projectId = $stateParams.projectId;
	  $scope.description = "";
	  $scope.project = ProjectService.get($stateParams.projectId);

	  /* 
	   * Handle submitForm
	   */
	  $scope.submitForm = function() {

	  	var trueState = {
	    	'order': 'time',
	    	'surgeon': 'expense'
	    };

	  	var newExp = {};
	  	var newOrder = {};

	  	if (trueState[$stateParams.type] === 'expense') {
		    newExp = {
		      "mobilecaddy1__Short_Description__c": $scope.forms.expenseForm.description.$modelValue,
		      "Name": 'TMP-' + Date.now(),
		      "mobilecaddy1__Project__c": $stateParams.projectId
		    };
		} else {

	      	var surgeonId = OrderService.getSelectedSurgeon().Name;
	      	var productId = OrderService.getSelectedProduct().productId;


			newExp = {
		      "mobilecaddy1__Short_Description__c": JSON.stringify({
		      	sId: surgeonId,
		      	pId: productId
		      }),
		      "Name": 'TMP-' + Date.now(),
		      "mobilecaddy1__Project__c": $stateParams.projectId
		    };
		}

	    switch (trueState[$stateParams.type]) {
	      case 'time' :
	        newExp.mobilecaddy1__Duration_Minutes__c = 0;
	        newExp.mobilecaddy1__Expense_Type__c = 'time';
	        break;
	      default :
	        newExp.mobilecaddy1__Expense_Amount__c = 0;
	        newExp.mobilecaddy1__Expense_Type__c = $scope.forms.expenseForm.expenseType.$modelValue;
	    }
	    $ionicLoading.show({
	      duration: 30000,
	      delay : 400,
	      maxWidth: 600,
	      noBackdrop: true,
	      template: '<h1>Saving...</h1><p id="app-progress-msg" class="item-icon-left">Saving ' + trueState[$stateParams.type] + ' record...<ion-spinner/></p>'
	    });

	    console.log(newExp);

	    if (trueState[$stateParams.type] === 'expense') {
		    ProjectService.newExpense(newExp,
		      function(){
		        $ionicLoading.hide();
		        $rootScope.$broadcast('refreshProjectTotals');
		        $location.path("/tab/project/" + $stateParams.projectId);
		      },
		      function(e) {
		        console.error('NewExpenseCtrl, error', e);
		        $ionicLoading.hide();
		        var alertPopup = $ionicPopup.alert({
		          title: 'Insert failed!',
		          template: '<p>Sorry, something went wrong.</p><p class="error_details">Error: ' + e.status + ' - ' + e.mc_add_status + '</p>'
		        });
		      });
		} else {
			ProjectService.newExpense(newExp,
		      function(){
		        $ionicLoading.hide();
		        $rootScope.$broadcast('refreshProjectTotals');
		        $location.path("/tab/project/" + $stateParams.projectId);
		      },
		      function(e) {
		        console.error('NewExpenseCtrl, error', e);
		        $ionicLoading.hide();
		        var alertPopup = $ionicPopup.alert({
		          title: 'Insert failed!',
		          template: '<p>Sorry, something went wrong.</p><p class="error_details">Error: ' + e.status + ' - ' + e.mc_add_status + '</p>'
		        });
		      });
		}
	  };

	  $scope.scanImageData = null;

	  $scope.scanBarcode = function() {
	    if (cordova && cordova.plugins && cordova.plugins.barcodeScanner) {
	      $cordovaBarcodeScanner.scan().then(function(imageData) {
	        //console.log("Cancelled -> " + imageData.cancelled);
	        if (!imageData.cancelled) {
	          $scope.scanImageData = imageData;
	          //console.log("Barcode Format -> " + imageData.format);
	        }
	      }, function(error) {
	        console.error(err);
	      });
	    } else {
	      $scope.scanImageData = "0";
	    }

	    var product = ProductService.get($scope.scanImageData);

	    if (product) {
	    	OrderService.setSelectedProduct(product);
	    	$scope.selectedProduct = OrderService.getSelectedProduct().name;
	    }
	  };

	  $scope.photoImageData = null;

	  $scope.capturePhoto = function() {
	    Camera.getPicture().then(function(imageData) {
	      //console.log('capturePhoto success');
	      $scope.photoImageData = imageData;
	    }, function(err) {
	      console.error(err);
	    });
	  };

  }

})();