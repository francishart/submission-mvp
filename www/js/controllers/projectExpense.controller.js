/**
 * ProjectExpense Controller
 *
 * @description controller for the expenses listing
 */
(function() {
  'use strict';

  angular
    .module('starter.controllers')
    .controller('ProjectExpenseCtrl', ProjectExpenseCtrl);

  ProjectExpenseCtrl.$inject = ['$scope', '$stateParams', 'ProjectService', 'ProductService'];

  function ProjectExpenseCtrl($scope, $stateParams, ProjectService, ProductService) {

  		console.log("State Param: " + $stateParams.type);
	  switch ($stateParams.type) {
	      case 'surgeon' : $scope.paramType = "Surgeons";
	      	break;
	      case 'order' : $scope.paramType = "Orders";
	        break;
	      default :  $scope.paramType = 'Other';
	    }

	  ProjectService.expenses($stateParams.type, $stateParams.projectId).then(function(timesheets) {
	  	if ($stateParams.type === 'order') {
	  		ProjectService.expenses('surgeon', $stateParams.projectId).then(function(surgeons) {
	  			$scope.surgeons = surgeons;
	  			for (var i = 0; i < timesheets.length; i++) {
	  				if (timesheets[i].mobilecaddy1__Short_Description__c) {
	  					var ids = JSON.parse(timesheets[i].mobilecaddy1__Short_Description__c);

				        var surgeon =  _.where(surgeons, {'Name': ids.sId});
	      				surgeon = surgeon[0];

				        timesheets[i].product = ProductService.get(ids.pId);
				        timesheets[i].surgeon = surgeon;
		  			}

		  			if (timesheets[i].LastModifiedDate) {
	  					timesheets[i].LastModifiedDate = new Date(Date.parse(timesheets[i].LastModifiedDate)).toString();
					}
		  		}
		  		$scope.expenses = timesheets;
		      }, function(reason) {
		        console.error('promise returned error, reason -> ' + reason);
		      });
	  	} else {
	    	$scope.expenses = timesheets;
	    }
	  }, function(reason) {
	    console.error('promise returned error, reason -> ' + reason);
	  });

  }

})();