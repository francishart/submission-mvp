/**
 * SelectSurgeon Controller
 *
 * @description controller for new expenses
 */
(function() {
  'use strict';

  angular
    .module('starter.controllers')
    .controller('SelectSurgeonCtrl', SelectSurgeonCtrl);

  SelectSurgeonCtrl.$inject = ['$scope', '$rootScope', '$stateParams', '$ionicLoading', '$ionicPopup', '$ionicModal', '$location', '$cordovaBarcodeScanner', 'ProjectService', 'Camera', 'OrderService'];

  function SelectSurgeonCtrl($scope, $rootScope, $stateParams, $ionicLoading, $ionicPopup, $ionicModal, $location, $cordovaBarcodeScanner, ProjectService, Camera, OrderService) {

      var winHeight = window.innerHeight - 125;
      var surgeonsList = document.getElementById('surgeon-list');
      surgeonsList.setAttribute("style","height:" + winHeight + "px");

      $scope.surgeons = [
        {
          name: 'Bob',
          title: 'FFFF'
        },
        {
          name: 'Ted',
          title: 'FFFF'
        },
        {
          name: 'Cat',
          title: 'FFFF'
        },
        {
          name: 'Hat',
          title: 'FFFF'
        },
        {
          name: 'Dig',
          title: 'FFFF'
        }
      ];

      $scope.setSelectedSurgeonAndReturn = function (selection) {
        console.log("Selection: " + JSON.stringify(selection));
        OrderService.setSelectedSurgeon(selection);
        console.log("OrderService: " + JSON.stringify(OrderService.setSelectedSurgeon));
        $rootScope.$broadcast('surgeon-updated');
        $location.path('tab/project/order/new/' + $stateParams.projectId);
      };

      ProjectService.expenses('surgeon', $stateParams.projectId).then(function(timesheets) {
        $scope.expenses = timesheets;
      }, function(reason) {
        console.error('promise returned error, reason -> ' + reason);
      });

  }

})();