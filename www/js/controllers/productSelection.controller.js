/**
 * SelectProduct Controller
 *
 * @description controller for new expenses
 */
(function() {
  'use strict';

  angular
    .module('starter.controllers')
    .controller('SelectProductCtrl', SelectProductCtrl);

  SelectProductCtrl.$inject = ['$scope', '$rootScope', '$stateParams', '$ionicLoading', '$ionicPopup', '$ionicModal', '$location', '$cordovaBarcodeScanner', 'ProjectService', 'Camera', 'OrderService', 'ProductService'];

  function SelectProductCtrl($scope, $rootScope, $stateParams, $ionicLoading, $ionicPopup, $ionicModal, $location, $cordovaBarcodeScanner, ProjectService, Camera, OrderService, ProductService) {

      var winHeight = window.innerHeight - 125;
      var productsList = document.getElementById('product-list');
      productsList.setAttribute("style","height:" + winHeight + "px");


      $scope.setSelectedProductAndReturn = function (selection) {
        OrderService.setSelectedProduct(selection);
        $rootScope.$broadcast('product-updated');
        $location.path('tab/project/order/new/' + $stateParams.projectId);
      };

      ProductService.all(false, null).then(function(products) {
        $rootScope.products = products;
        //console.log('ProductIndexCtrl, got products');
        $ionicLoading.hide();
        syncButtonsClass("Remove", "ng-hide");
      }, function(reason) {
        //console.log('promise returned reason -> ' + reason);
      });

  }

})();